import { Component, h, Prop } from '@stencil/core';


@Component({
  tag: 'render-number',
  // styleUrl: 'render-number.css'
})
export class RenderNumber {
  @Prop() number: number;
  render() {
    return (
      <p>My name is Jump {this.number}</p>
    );
  }
}