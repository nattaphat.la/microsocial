import { Component, Prop, h, State, Watch, EventEmitter, Event } from '@stencil/core';
import { format } from '../../utils/utils';

@Component({
  tag: 'my-component',
  styleUrl: 'my-component.css',
  shadow: true
})
export class MyComponent {
  /**
   * The first name
   */
  @Prop() first: string;

  /**
   * The middle name
   */
  @Prop() middle: string;

  /**
   * The last name
   */
  @Prop() last: string;

  @State() count: number = 0;

  @Event() onIncremented: EventEmitter;

  @Watch('count')
  countHandler(newValue, oldValue) {
    console.log('count change', newValue, oldValue)
  }
  private getText(): string {
    return format(this.first, this.middle, this.last);
  }

  increment = () => {
    this.count = this.count + 1
    this.onIncremented.emit(this.count)
  }

  render() {
    return (
      <div>
        <p>Hello22222, World! I'm {this.getText()} {this.count}</p>
        <button onClick={this.increment}>increment</button>
      </div>
    );
  }
}
