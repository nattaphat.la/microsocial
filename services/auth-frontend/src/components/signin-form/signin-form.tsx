import { Component, h, Event, EventEmitter, State, Prop, Element } from '@stencil/core';
import config from '../../utils/config'
import resource from '../../utils/resource'
import Tunnel from '../auth-provider/data/auth'

@Component({
  tag: 'signin-form',
  styleUrl: 'signin-form.css'
})
export class SigninForm {
  @Element() el: SigninForm
  @State() formData: object;
  @Prop() apiUrl: string = config.API_URL
  @Prop() setToken: (token) => void;
  @Event() signinSuccess: EventEmitter;
  @Event() signinFail: EventEmitter;
  
  handleSubmit = async (e) => {
    e.preventDefault()
    try {
      const response = await resource.post('/appusers/login', this.formData)
      this.signinSuccess.emit(response.data)
      this.setToken(response.data.accessToken)
      return Promise.resolve(response.data)
    } catch (e) {
      this.signinFail.emit(e)
      return Promise.reject(e)
    }
  }
  handleChange = (e) => {
    this.formData = {
      ...this.formData,
      [e.target.name]: e.target.value
    }
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text" name="username" onInput={this.handleChange}/>
        <input type="password" name="password" onInput={this.handleChange}/>
        <button type="submit">Submit</button>
      </form>
    );
  }
}
Tunnel.injectProps(SigninForm, ['setToken']);