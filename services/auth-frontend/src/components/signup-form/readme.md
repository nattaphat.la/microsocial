# signup-form



<!-- Auto Generated Below -->


## Events

| Event           | Description | Type               |
| --------------- | ----------- | ------------------ |
| `signupFail`    |             | `CustomEvent<any>` |
| `signupSuccess` |             | `CustomEvent<any>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
