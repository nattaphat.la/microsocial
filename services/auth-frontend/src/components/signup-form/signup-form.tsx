import { Component, h, Event, EventEmitter, State, Prop } from '@stencil/core';
import config from '../../utils/config'
import resource from '../../utils/resource'
@Component({
  tag: 'signup-form',
  styleUrl: 'signup-form.css'
})
export class SignupForm {
  @State() formData: object;
  @Prop() apiUrl: string = config.API_URL
  @Event() signupSuccess: EventEmitter;
  @Event() signupFail: EventEmitter;
  
  handleSubmit = async (e) => {
    e.preventDefault()
    try {
      const response = await resource.post('/appusers', this.formData)
      this.signupSuccess.emit(response.data)
      return Promise.resolve(response.data)
    } catch (e) {
      this.signupFail.emit(e)
      return Promise.reject(e)
    }
  }
  handleChange = (e) => {
    this.formData = {
      ...this.formData,
      [e.target.name]: e.target.value
    }
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text" name="firstname" onInput={this.handleChange}/>
        <input type="text" name="lastname" onInput={this.handleChange}/>
        <input type="text" name="username" onInput={this.handleChange}/>
        <input type="text" name="email" onInput={this.handleChange}/>
        <input type="password" name="password" onInput={this.handleChange}/>
        <button type="submit">Submit</button>
      </form>
    );
  }
}