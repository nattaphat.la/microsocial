import { h } from '@stencil/core';
import { createProviderConsumer } from '@stencil/state-tunnel';

export interface State {
  user?: object,
  token?: string,
  setToken?: (token) => void,
  setUser?: (user) => void,
}

export default createProviderConsumer<State>({
    // message: 'Hello!'
  },
  (subscribe, child) => (
    <context-consumer subscribe={subscribe} renderer={child} />
  )
);