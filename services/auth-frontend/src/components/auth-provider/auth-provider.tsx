import { Component, h, State, Watch, Method } from '@stencil/core';
import Tunnel from './data/auth'
import request from '../../utils/resource'
@Component({
  tag: 'auth-provider',
  styleUrl: 'auth-provider.css',
  shadow: true
})
export class AuthProvider {
  @State() user: object;
  @State() token: string;
  @State() isLoading: boolean = true;
  connectedCallback() {
    this.token = localStorage.getItem('token')
    if (!this.token) {
      this.isLoading = false
    }
  }
  @Watch('token')
  // onTokenUpdate(newValue: string, oldValue: string) {
  async onTokenUpdate(newValue: string) {
    if (!newValue) {
      return
    }
    // console.log('The new value of token is: ', newValue, oldValue);
    try {
      request.defaults.headers.common['Authorization'] = newValue;
      const res = await request.get('/appusers/me')
      this.setUser(res.data)
    } catch (e) {

    }
    this.isLoading = false
  }

  @Method()
  async setToken(token) {
    console.log(this)
    localStorage.setItem('token', token);
    this.token = token
  }
  setUser = (user) => {
    this.user = {...user}
  }
  @Method()
  async logout() {
    console.log(this)
    localStorage.removeItem('token')
    this.token = undefined
    this.user = undefined
  }
  
  render() {
    const tunnelState = {
      user: this.user,
      token: this.token,
      setUser: this.setUser.bind(this),
      setToken: this.setToken.bind(this)
    }
    // console.log('test', tunnelState)
    if (this.isLoading) {
      return <div>Loading</div>
    }
    return (
      <Tunnel.Provider state={tunnelState}>
        {this.user ? <slot name="authenticated"/> : <slot name="unauthenticated"/>}
      </Tunnel.Provider>
    );
  }
}