import axios from 'axios'
import config from './config'
const instance = axios.create({
  baseURL: config.API_URL,
  // headers: {
  //   Authorization: null
  // }
})
export default instance