import _ from 'lodash'
const config = {
  API_URL: _.get(process.env, 'API_URL', 'http://localhost/api')
}

export default config;