import { Config } from '@stencil/core';
import replacePlugin from 'rollup-plugin-replace'
export const config: Config = {
  namespace: 'auth-frontend',
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader'
    },
    {
      type: 'docs-readme'
    },
    {
      type: 'www',
      serviceWorker: null // disable service workers
    }
  ],
  plugins: [
    replacePlugin({
      'process.env': JSON.stringify(process.env)
    })
  ]
};
