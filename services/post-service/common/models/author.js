'use strict';

module.exports = function(Author) {
  Author.consumeItems = async ({id, ...payload}) => {
    // console.log('consume all item', payload);
    const data = {
      reference: id,
      ...payload,
    };
    try {
      await Author.upsertWithWhere({
        reference: data.reference,
      }, data);
      return Promise.resolve();
    } catch (e) {
      console.debug('upsert author fail', payload, e);
      return Promise.reject(e);
    }
  };
};
