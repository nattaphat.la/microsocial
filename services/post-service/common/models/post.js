'use strict';

module.exports = function(Post) {
  // auto binding author
  Post.observe('before save', async (ctx) => {
    const {
      Author,
    } = Post.app.models;
    if (!ctx.isNewInstance) {
      return Promise.resolve();
    }
    try {
      // already pass acl, userId should now exist
      const {
        userId,
        firstname,
        lastname,
      } = ctx.options.accessToken;
      // find or create author by userId
      const [author] = await Author.findOrCreate({
        where: {
          reference: userId.toString(),
        },
      }, {
        firstname,
        lastname,
        reference: userId.toString(),
      });
      ctx.instance.authorId = author.id;
      return Promise.resolve();
    } catch (e) {
      console.debug('Error while creating post', e);
      return Promise.reject(e);
    }
  });

  // publish event
  Post.observe('after save', async ctx => {
    try {
      console.log('Item after save. ctx.instance: %o', ctx.instance);
      return await Post.publishItem(ctx.instance);
    } catch (e) {
      console.debug('publish fail', e);
      if (e.message.includes('Publish failed')) {
        return Promise.resolve();
      }
      return Promise.reject(e);
    }
  });
};
