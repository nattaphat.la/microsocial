'use strict';

module.exports = {
  'loopback-component-mq/lib': process.env.RABBIT_URI && {
    'topology': {
      'connection': {
        'uri': process.env.RABBIT_URI,
      },
      'exchanges': [{
        'name': 'post.write',
        'type': 'fanout', // fanout to every queue
        'persistent': true,
      }, {
        'name': 'user.write',
        'type': 'fanout', // fanout to every queue
        'persistent': true,
      }],
      'queues': [{
        'name': 'post.user.write',
        'subscribe': true,
      }],
      'bindings': [{
        'exchange': 'user.write',
        'target': 'post.user.write',
        'keys': ['#'],
      }],
    },
  },
};
