'use strict';

module.exports = function(Content) {
  Content.consumeItems = async ({id, ...payload}) => {
    // console.log('consume all item', payload);
    const data = {
      reference: id,
      ...payload,
    };
    try {
      await Content.upsertWithWhere({
        reference: data.reference,
      }, data);
      return Promise.resolve();
    } catch (e) {
      console.debug('upsert content fail', payload, e);
      return Promise.reject(e);
    }
  };
};
