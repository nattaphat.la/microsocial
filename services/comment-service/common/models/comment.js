'use strict';

module.exports = function(Comment) {
  // auto binding author
  Comment.observe('before save', async (ctx) => {
    const {
      Author,
    } = Comment.app.models;
    if (!ctx.isNewInstance) {
      return Promise.resolve();
    }
    try {
      // already pass acl, userId should now exist
      const {
        userId,
        firstname,
        lastname,
      } = ctx.options.accessToken;
      // find or create author by userId
      const [author] = await Author.findOrCreate({
        where: {
          reference: userId.toString(),
        },
      }, {
        firstname,
        lastname,
        reference: userId.toString(),
      });
      ctx.instance.authorId = author.id;
      return Promise.resolve();
    } catch (e) {
      console.debug('Error while creating comment', e);
      return Promise.reject(e);
    }
  });
};
