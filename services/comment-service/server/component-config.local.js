'use strict';

module.exports = {
  'loopback-component-mq/lib': process.env.RABBIT_URI && {
    'topology': {
      'connection': {
        'uri': process.env.RABBIT_URI,
      },
      'exchanges': [{
        'name': 'user.write',
        'type': 'fanout',
        'persistent': true,
      }, {
        'name': 'post.write',
        'type': 'fanout',
        'persistent': true,
      }],
      'queues': [{
        'name': 'comment.user.write',
        'subscribe': true,
      }, {
        'name': 'comment.post.write',
        'subscribe': true,
      }],
      'bindings': [{
        'exchange': 'user.write',
        'target': 'comment.user.write',
        'keys': ['#'],
      }, {
        'exchange': 'post.write',
        'target': 'comment.post.write',
        'keys': ['#'],
      }],
    },
  },
};
