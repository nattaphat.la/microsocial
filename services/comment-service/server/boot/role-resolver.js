// Copyright IBM Corp. 2015,2019. All Rights Reserved.
// Node module: generator-loopback
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

'use strict';

module.exports = function(app) {
  var Role = app.models.Role;
  Role.registerResolver('author', async (role, context) => {
    const {
      model: Model,
      modelId,
    } = context;
    if (!context.accessToken.userId) { // not login should return false
      return Promise.resolve(false);
    }
    if (!modelId) { // skip if modelId not found -> create operation
      return Promise.resolve(true);
    }
    const instance = await Model.findById(parseInt(modelId));
    if (!instance.author) { // model not have relation with author
      return Promise.resolve(true);
    }
    const author = await instance.author.get();
    if (!author) { // model have relation with author but not connect with author
      return Promise.resolve(true);
    }
    const {
      reference: userId,
    } = author;
    if (parseInt(userId) === context.accessToken.userId) { // author match with access token
      return Promise.resolve(true);
    }
    return Promise.resolve(false);
  });
};
