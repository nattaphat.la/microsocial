'use strict';

module.exports = function(Appuser) {
  Appuser.observe('after save', async ctx => {
    try {
      console.log('Item after save. ctx.instance: %o', ctx.instance);
      return await Appuser.publishItem(ctx.instance);
    } catch (e) {
      console.debug('publish fail', e);
      if (e.message.includes('Publish failed')) {
        return Promise.resolve();
      }
      return Promise.reject(e);
    }
  });
};
