'use strict';

module.exports = {
  'loopback-component-mq/lib': process.env.RABBIT_URI && {
    'topology': {
      'connection': {
        'uri': process.env.RABBIT_URI,
      },
      'exchanges': [{
        'name': 'user.write',
        'type': 'fanout', // fanout to every queue in exchange
        'persistent': true,
      }],
      'queues': [],
      'bindings': [],
    },
  },
};
