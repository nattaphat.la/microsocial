# Usage
## Install via yarn
```
yarn add loopback-component-jwt
```

## Configuration
add component in server/component-config.json
```
"loopback-component-jwt": {
  "userModel": "User",
  "accessTokenModel": "AccessToken"
  "fields": [
    "firstname",
    "lastname"
  ],
  "secretKey": "mysecretkey"
}
```

