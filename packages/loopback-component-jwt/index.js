const loopback = require('loopback');
const jwt = require('jsonwebtoken');
module.exports = function (loopbackApplication, options) {
  const config = Object.assign({
    userModel: 'User',
    accessTokenModel: 'AccessToken',
    secretKey: process.env.JWT_SECRET_KEY || 'mysecretkey',
    fields: [],
  }, options)
  const User = loopbackApplication.models[config.userModel];
  const AccessToken = loopbackApplication.models[config.accessTokenModel];
  loopbackApplication.middleware('auth', loopback.token({
    model: AccessToken,
    currentUserLiteral: 'me',
    bearerTokenBase64Encoded: false // here
  }));

  User.prototype.createAccessToken = function(ttl, cb) {
    const userSettings = this.constructor.settings;
    const expiresIn = Math.min(ttl || userSettings.ttl, userSettings.maxTTL);
    let tokenPayload = {
      id: this.id,
    }
    tokenPayload = config.fields.reduce((result, field) => {
      let data = Object.assign({}, result);
      if (this[field]) {
        data = Object.assign(data, {
          [field]: this[field]
        })
      }
      return data;
    }, tokenPayload)
    const accessToken = jwt.sign(tokenPayload, config.secretKey, {expiresIn});
    return cb ? cb(null, Object.assign(this, {accessToken})) : {id: accessToken};
  };
  User.logout = function(tokenId, fn) {
    // You may want to implement JWT blacklist here
    // TODO
    fn();
  };

  AccessToken.resolve = function(id, cb) {
    if (id) {
      try {
        const tokenData = jwt.verify(id, config.secretKey);
        let result = {userId: tokenData.id}
        result = config.fields.reduce((r, field) => {
          let data = Object.assign({}, r)
          if (tokenData[field]) {
            data = Object.assign(data, {
              [field]: tokenData[field]
            })
          }
          return data;
        }, result)
        cb(null, result);
      } catch (err) {
        // Should override the error to 401
        cb(err);
      }
    } else {
      cb();
    }
  };
};